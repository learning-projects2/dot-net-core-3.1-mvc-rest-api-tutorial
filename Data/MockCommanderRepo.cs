using System.Collections.Generic;
using Commander.Models;

namespace Commander.Data 
{
    public class MockCommanderRepo : ICommanderRepo
    {
        public void CreateCommand(Command cmd)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteCommand(Command cmd)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Command> GetAllCommands()
        {
            var commands = new List<Command>
            {
                new Command{Id=0, HowTo="Boil an egg1", Line="Boil Water1", Platform="Kettle & Pan1"},
                new Command{Id=1, HowTo="Boil an egg2", Line="Boil Water2", Platform="Kettle & Pan2"},
                new Command{Id=2, HowTo="Boil an egg3", Line="Boil Water3", Platform="Kettle & Pan3"},
                new Command{Id=3, HowTo="Boil an egg4", Line="Boil Water4", Platform="Kettle & Pan4"}
            };

            return commands;
        }

        public Command GetCommandById(int id)
        {
            return new Command{Id=0, HowTo="Boil an egg", Line="Boil Water", Platform="Kettle & Pan"};
        }

        public bool SaveChanges()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateCommand(Command cmd)
        {
            throw new System.NotImplementedException();
        }
    }
}