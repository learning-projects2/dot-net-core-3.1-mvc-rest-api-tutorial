Following this tutorial: https://www.youtube.com/watch?v=fmvcAzHpsk8

The purpose of this project is to learn how to build a full REST API using .NET Core 3.1 using MVC, REST, Repository Pattern, Dependancy Injection, Entity Framework, Data Transfer Objects and Automapper.